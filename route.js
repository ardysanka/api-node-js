// file name: route.js 
// description: for Routing API
// initialize express router
let router = require('express').Router();
// set default API Response 
router.get("/",function (req,res) {
    var fruits = ["Apple", "Banana"];
    res.json({
        status:'API IS WORKING',
        author:'lufrey',
        test:fruits,
        message:'Welcome To my API'
    })
    
});
// Import contact controller
var contactController = require('./controllers/ControllerContact');
// Contact routes
router.route('/contacts')
    .get(contactController.index)
    .post(contactController.new);
router.route('/contacts/:contact_id')
    .get(contactController.view)
    .patch(contactController.update)
    .put(contactController.update)
    .delete(contactController.delete);
// Export API routes
module.exports = router;